package org.nrg.xnatx.plugins.metrics;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitJupiterConfig;

@ExtendWith(SpringExtension.class)
@SpringJUnitJupiterConfig(XnatMetricsPluginTest.class)
@Configuration
@Slf4j
public class XnatMetricsPluginTest {
    @Test
    public void sanityCheck() {
        log.info("This is a sanity check test. It should execute and exit successfully.");
    }
}
