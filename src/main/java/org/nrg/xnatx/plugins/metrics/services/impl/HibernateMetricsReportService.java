package org.nrg.xnatx.plugins.metrics.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.metrics.daos.MetricsReportDao;
import org.nrg.xnatx.plugins.metrics.dto.ReportData;
import org.nrg.xnatx.plugins.metrics.entities.MetricsReport;
import org.nrg.xnatx.plugins.metrics.services.MetricsReportEntityService;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class HibernateMetricsReportService extends AbstractHibernateEntityService<MetricsReport, MetricsReportDao>
	implements MetricsReportEntityService {
    
    @Override
    public List<MetricsReport> findAllReports() {
    	return getDao().findAllReports();
    }

    @Override
    public MetricsReport findReportById(final long id) {
    	return getDao().retrieve(id);
    }

    @Override
    public MetricsReport createNewReport(final MetricsReport report) {
    	return super.create(report);
    }
}
