package org.nrg.xnatx.plugins.metrics.services;

import java.util.Base64;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.nrg.xnatx.plugins.metrics.dto.PharosReportDto;
import org.nrg.xnatx.plugins.metrics.dto.ReportData;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Metrics Transmission service. Sends reports to Metrics Collector Service.
 *
 */
@Service
@Slf4j
public class MetricsTransmissionService {

    /**
     * Creates new instance of MetricsTransmission Service.
     * 
     * @param restTemplate Spring's RestTemplate.
     * 
     */
    @Autowired
    public MetricsTransmissionService(final RestTemplate restTemplate, final MetricsPreferencesBean metricsPreferencesBean) {
	_restTemplate = restTemplate;
	headers.add("Authorization","Basic " + new String(Base64.getEncoder().encode(BASIC_HTTP_CREDENTIALS.getBytes())));
	headers.setContentType(MediaType.APPLICATION_JSON);
	_preferences = metricsPreferencesBean;

    }

    /**
     * Sends the report to the Metrics Collector Service
     * 
     * @param reportData
     * 
     * @return {@link ResponseEntity<String> responseEntity}
     */
    public ResponseEntity<String> pushReportToAccessPoint(final List<Object> reportData) {
	try {
	    final HttpEntity<PharosReportDto> httpEntity = new HttpEntity<>(createPharosReport(reportData), headers);
	    return _restTemplate.postForEntity(ACCESS_POINT_URL, httpEntity, String.class);
	} catch (RestClientException exception) {
	    log.error(exception.getMessage());
	    return null;
	}
    }

    /**
     * Prepares the a PharosReportDto from ReportData
     * 
     * @param {@link ReportData reportData}
     * 
     * @return {@link PharosReportDto pharosReportDto}
     */
    private PharosReportDto createPharosReport(final List<Object> reportData) {
	final PharosReportDto.Builder builder = PharosReportDto.builder();
	return builder.uuid(_preferences.getSiteUuid()).version("1.7.6").ipAddress("10.1.1.1").reportData(reportData)
		.build();
    }

    public String getSiteUuidFromPharos() {
	final HttpEntity<String> httpEntity = new HttpEntity<>(headers);
	return _restTemplate.exchange(ACCESS_POINT_SITE_UUID, HttpMethod.GET, httpEntity, String.class).getBody().replace("\"", "");
    }

    private final HttpHeaders headers = new HttpHeaders();
    private final static String ACCESS_POINT_SITE_UUID = "http://10.0.2.2:9000/app/register";
    private final static String ACCESS_POINT_URL = "http://10.0.2.2:9000/metrics";
    private final static String BASIC_HTTP_CREDENTIALS = "da05abf7-36ba-4f56-a094-c0475222ede3:password";
    private final RestTemplate _restTemplate;
    private final MetricsPreferencesBean _preferences;

}
