package org.nrg.xnatx.plugins.metrics.providers;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Interface to be implemented by providers.
 *
 */
public interface MetricsProvider {

    /**
     * @return {@link String id} of the Metrics Provider.
     */
    String getId();
    
    /**
     * @return {@link String version} of the Metrics Provider.
     */
    String getVersion();
    
    /**
     * @return {@link URL url} of the Metrics Provider.
     */
    URL getUrl();
    
    
    /**
     * @return Metrics of the MetricsProvider.
     * @throws Exception
     */
    Object getMetrics() throws Exception;
    
}
