package org.nrg.xnatx.plugins.metrics.component;

import lombok.extern.slf4j.Slf4j;
import org.nrg.xnatx.plugins.metrics.preferences.MetricsPreferencesBean;
import org.nrg.xnatx.plugins.metrics.services.MetricsTransmissionService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Initialization implements InitializingBean {
    private final MetricsPreferencesBean _metricsPreferencesBean;
    private final MetricsTransmissionService _metricsTransmissionService;

    @Autowired
    public Initialization(MetricsPreferencesBean metricsMetricsPreferencesBean,
	    MetricsTransmissionService metricsTransmissionService) {
	_metricsPreferencesBean = metricsMetricsPreferencesBean;
	_metricsTransmissionService = metricsTransmissionService;
    }

    @Override
    public void afterPropertiesSet() {
	getSiteUuidfromAccessPoint();
    }

    private void getSiteUuidfromAccessPoint() {
	final String siteUuid = _metricsPreferencesBean.getSiteUuid();
	if (siteUuid == null || siteUuid.equals("") || siteUuid.equals("Unknown")) {
	    try {
		_metricsPreferencesBean.setSiteUuid(_metricsTransmissionService.getSiteUuidFromPharos());
		log.info("SiteUuid retrieved from pharos.");
	    } catch (Exception e) {
		log.error("Something went wrong while getting SiteUuid from Pharos" + e.getMessage());
	    }
	}
    }

  
}
